# Ender 6 custom configuration
# Author: Tim Jones, 2021

# X Axis
[stepper_x]
step_pin: PB8
dir_pin: PB7
enable_pin: !PC3
microsteps: 16
rotation_distance: 40
endstop_pin: ^PA5
position_endstop: 260
position_max: 260
homing_speed: 50

# Y Axis
[stepper_y]
step_pin: PC2
dir_pin: !PB9
enable_pin: !PC3
microsteps: 16
rotation_distance: 40
endstop_pin: ^PA6
position_endstop: 260
position_max: 260
homing_speed: 50

# Z Axis
[stepper_z]
step_pin: PB6
dir_pin: PB5
enable_pin: !PC3
microsteps: 16
rotation_distance: 8
endstop_pin: probe:z_virtual_endstop 
position_min: -5
position_max: 400

[safe_z_home]
home_xy_position: 150.7, 137
speed: 100
z_hop: 10
z_hop_speed: 5

# Extruder
[extruder]
max_extrude_only_distance: 1000.0
step_pin: PB4
dir_pin: !PB3
enable_pin: !PC3
microsteps: 16
pressure_advance: 0.96
rotation_distance: 22.39986
nozzle_diameter: 0.400
filament_diameter: 1.750
heater_pin: PA1
sensor_type: EPCOS 100K B57560G104F
sensor_pin: PC5
#control: pid
#pid_Kp: 26.949
#pid_Ki: 1.497
#pid_Kd: 121.269
min_temp: 0
max_temp: 270
#min_extrude_temp: 0 # For calibrating e-steps

# BLTouch Probe
[bltouch]
sensor_pin: ^PB1
control_pin: PB0
x_offset: 20.7
y_offset: 7
#z_offset: 0
speed: 3.0

[bed_mesh]
speed: 100
mesh_min: 35, 10
mesh_max: 275, 265
algorithm: bicubic
probe_count: 5,5
horizontal_move_z: 10

[heater_bed]
heater_pin: PA2
sensor_type: EPCOS 100K B57560G104F
sensor_pin: PC4
#control: pid
#pid_Kp: 327.11
#pid_Ki: 19.20
#pid_Kd: 1393.45
min_temp: 0
max_temp: 120

[fan]
pin: PA0

# Main controller board MCU
[mcu]
serial: /dev/serial/by-id/usb-1a86_USB_Serial-if00-port0
restart_method: command

# Raspberry Pi as secondary MCU
[mcu rpi]
serial: /tmp/klipper_host_mcu

[printer]
kinematics: corexy
max_velocity: 500
max_accel: 2000
max_z_velocity: 10
max_z_accel: 100

[firmware_retraction]
retract_length: 6
retract_speed: 30

[resonance_tester]
accel_chip: adxl345
probe_points:
    130,130,20

# Accellerometer
[adxl345]
cs_pin: rpi:None

# Display settings
[t5uid1]
firmware: dgus_reloaded
machine_name: Creality Ender-6
#volume: 0
brightness: 100
z_min: 0
z_max: 400

[filament_switch_sensor e0_sensor]
switch_pin: PA4

[virtual_sdcard]
path: ~/gcode_files

[display_status]

[pause_resume]

[gcode_arcs]

[idle_timeout]

[gcode_macro LAZY_HOME]
description: Home axes only if needed
gcode:
	{% if printer.homed_axes != 'XYZ' %}
		G28 
	{% endif %}


[gcode_macro WIPE_LINE]
description: Extrude a nozzle wipe line
gcode:
    {% set x = params.X|default(5)|float %}
    {% set y1 = params.Y1|default(5)|float %}
    {% set y2 = params.Y2|default(200)|float %}
    {% set z = params.Z|default(0.20)|float %}
    {% set temp = params.TEMP|default(190)|float %}

    {% if printer.extruder.temperature < (temp-5) %}
        {action_respond_info("Extruder temperature too low")}

    {% else %}
        SAVE_GCODE_STATE NAME=WIPE_LINE_state
        
        G90 ; Absolute nozzle positioning
        
        M82    ; Absolute extruder positioning
        G92 E0 ; Reset extruder to 0

        G1 X{x} Y{y1} Z{z+6} F3000 ; Go to start position
        G1 Z{z} F3000              ; Lower nozzle

        G1 X{x} Y{y2} F1500 E{(y2-y1)/20} ; Draw wipe line 1
        G1 Y{y1} F1500 E{(y2-y1)/10}      ; Draw wipe line 2 (extrude more)

        G1 Z{z+6} F3000 ; Raise nozzle

        RESTORE_GCODE_STATE NAME=WIPE_LINE_state MOVE=0
    {% endif %}


[gcode_macro PARK_TOOLHEAD]
description: Park toolhead in top right position
gcode:
    SAVE_GCODE_STATE NAME=PARK_TOOLHEAD_state

    G91
    G1 Z5 F1000
    G90                # Absolute positioning    
    G1 X260 Y260 F5000 # Move nozzle to far right

    RESTORE_GCODE_STATE NAME=PARK_TOOLHEAD_state MOVE=0 


[gcode_macro WIPE_FINISH]
description: Wipe off print 
gcode:
    {% set temp = params.TEMP|default(190)|float %}

    {% if printer.extruder.temperature < (temp-5) %}
        {action_respond_info("Extruder temperature too low")}

    {% else %}
        SAVE_GCODE_STATE NAME=WIPE_FINISH_state

        G91               # Relative positioning
        G1 E-10           # Retract a good bit
        G1 X-5 Y-5 F3000  # Move nozzle away from print

        RESTORE_GCODE_STATE NAME=WIPE_FINISH_state MOVE=0
    {% endif %}


[gcode_macro PRESENT_PRINT]
description: Present print to user
gcode:
    {% set z = params.Z|default(380)|float %}

    SAVE_GCODE_STATE NAME=PRESENT_PRINT_state
    
    {% if printer.gcode_move.position.z < z %}
        G1 Z{z} F5000
    {% endif %}

    RESTORE_GCODE_STATE NAME=PRESENT_PRINT_state MOVE=0


[gcode_macro START_PRINT]
gcode:
    {% set bed_temp = params.BED_TEMP|default(0)|float %}
    {% set extruder_temp = params.EXTRUDER_TEMP|default(190)|float %}
   
    # Set display to show print progress screen
    {% if 't5uid1' in printer %}
        DGUS_PRINT_START
    {% endif %}
    
    M104 S{(extruder_temp/3)*2} # Start heat extruder (avoid oozing by limiting temp)
    M140 S{bed_temp}                # Start heating bed
    
    G4 S10 # Wait 10 seconds for partial warmup

    LAZY_HOME
    BED_MESH_CALIBRATE

    M109 S{extruder_temp} # Wait for extruder to reach final temp
    M190 S{bed_temp}      # Wait for bed to reach temp
    
    WIPE_LINE

    G92 E0        # Reset Extruder
    G1 Z2.0 F3000 # Move Z Axis up
    

[gcode_macro END_PRINT]
gcode:
    # Set display back to showing normal menu
    {% if 't5uid1' in printer %}
        DGUS_PRINT_END
    {% endif %}

    TURN_OFF_HEATERS
    
    M107 # Turn off part cooling fan

    WIPE_AND_PARK
    PRESENT_PRINT

    M84 # Disable steppers

[gcode_macro CANCEL_PRINT]
description: Cancel the running print
rename_existing: CANCEL_PRINT_BASE
gcode:
    TURN_OFF_HEATERS

    M107 # Turn off part cooling fan
    M84  # Disable steppers 

    CANCEL_PRINT_BASE

#*# <---------------------- SAVE_CONFIG ---------------------->
#*# DO NOT EDIT THIS BLOCK OR BELOW. The contents are auto-generated.
#*#
#*# [extruder]
#*# control = pid
#*# pid_kp = 25.630
#*# pid_ki = 1.512
#*# pid_kd = 108.605
#*#
#*# [heater_bed]
#*# control = pid
#*# pid_kp = 64.462
#*# pid_ki = 0.843
#*# pid_kd = 1232.830
#*#
#*# [t5uid1]
#*# volume = 55
#*#
#*# [bltouch]
#*# z_offset = 3.700
#*#
#*# [bed_mesh default]
#*# version = 1
#*# points =
#*# 	-0.052500, 0.005000, -0.015000, -0.062500, -0.110000
#*# 	-0.070000, -0.005000, -0.012500, -0.060000, -0.155000
#*# 	-0.102500, -0.017500, -0.030000, -0.042500, -0.145000
#*# 	-0.125000, -0.042500, -0.045000, -0.065000, -0.152500
#*# 	-0.080000, -0.052500, -0.047500, -0.087500, -0.135000
#*# tension = 0.2
#*# min_x = 35.0
#*# algo = bicubic
#*# y_count = 5
#*# mesh_y_pps = 2
#*# min_y = 10.0
#*# x_count = 5
#*# max_y = 265.0
#*# mesh_x_pps = 2
#*# max_x = 275.0
